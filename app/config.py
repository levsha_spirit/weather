import os
import dotenv


dotenv.load_dotenv()

API_KEY  = os.environ['API_KEY']
TEMPLATE_FOLDER = os.path.abspath('templates')
STATIC_FOLDER = os.path.abspath('static')