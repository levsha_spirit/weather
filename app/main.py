import flask
import config
import pyowm
from pyowm.utils.config import get_default_config


app = flask.Flask(
    __name__, template_folder=config.TEMPLATE_FOLDER, static_folder=config.STATIC_FOLDER
)


@app.route("/", methods=["GET"])
def get_form():
    return flask.render_template("index.html")


@app.route("/weather", methods=["POST"])
def get_weather():

    config_dict = get_default_config()
    config_dict["language"] = "ru"

    owm = pyowm.OWM(config.API_KEY)

    city = flask.request.form.get("city",)

    mgr = owm.weather_manager()

    try:
        observation = mgr.weather_at_place(city)
    except pyowm.commons.exceptions.NotFoundError:
        return flask.render_template('404.html')
    except pyowm.commons.exceptions.APIRequestError:
        return flask.render_template("400.html")

    w = observation.weather

    # средняя температура и по ощущениям
    t = w.temperature("celsius")
    t1 = t["temp"]
    t2 = t["feels_like"]
    # скорость ветра
    ws = w.wind()["speed"]
    # давление
    pr = w.pressure["press"] // 1.333
    # влажность
    humi = w.humidity
    # облачность
    # cl = w.clouds
    # статус
    detailed = w.detailed_status
    #видимость
    vis = w.visibility_distance // 1000

    return flask.render_template(
        "weather.html",
        temp=t1,
        sense=t2,
        windspeed=ws,
        press=pr,
        humidity=humi,
        city=city,
        detailed=detailed,
        visibility=vis
    )


if __name__ == "__main__":
    app.run()
